<?php
include_once APPPATH.'libraries/util/CI_Object.php';

    class ContaDataBuilder extends CI_Object{

        $contas = [[
            'parceiro' => 'Magalu', 
            'descricao' => 'Notebook', 
            'valor' => '2000', 
            'mes' => 1, 
            'ano' => 2021, 
            'tipo' => 'pagar'
            ],
            [
            'parceiro' => 'Casas Bahia', 
            'descricao' => 'Notebook', 
            'valor' => '3000', 
            'mes' => 1, 
            'ano' => 2021, 
            'tipo' => 'pagar'
            ],
            [
            'parceiro' => 'Salário', 
            'descricao' => 'Prefeitura de Sucupira', 
            'valor' => '3542.18', 
            'mes' => 1, 
            'ano' => 2021, 
            'tipo' => 'receber'
            ],
            [
            'parceiro' => 'Aluguel', 
            'descricao' => 'Casa de aluguel', 
            'valor' => '680', 
            'mes' => 1, 
            'ano' => 2021, 
            'tipo' => 'receber'
            ],
            [
            'parceiro' => 'Conta de Energia', 
            'descricao' => 'Consumo', 
            'valor' => '97.25', 
            'mes' => 1, 
            'ano' => 2021, 
            'tipo' => 'pagar'
            ]
            [
                'parceiro' => 'Conta de Energia', 
                'descricao' => 'Consumo', 
                'valor' => '87.25', 
                'mes' => 2, 
                'ano' => 2021, 
                'tipo' => 'pagar'
                ]
            ];

        public function start() {
            $this->load->library('conta');

            foreach ($contas as $conta) {
                $this->conta->cria($conta);
            }
        }

        public function clear() {
            $this->db->truncate('conta');
        }
    }
?>