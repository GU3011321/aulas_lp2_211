<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <!-- Container wrapper -->
  <div class="container-fluid">
    <!-- Navbar brand -->
    <a class="navbar-brand" href="<?= base_url('home') ?>">Controle Financeiro</a>

    <!-- Toggle button -->
    <button
      class="navbar-toggler"
      type="button"
      data-mdb-toggle="collapse"
      data-mdb-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <i class="fas fa-bars"></i>
    </button>

    <!-- Collapsible wrapper -->
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Navbar dropdown -->
        <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-mdb-toggle="dropdown"
            aria-expanded="false"
          >
            Cadastro
          </a>
          <!-- Dropdown menu -->
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="<?= base_url('usuario/cadastro') ?>">Usuário</a></li>
            <li><a class="dropdown-item" href="#">Conta Bancária</a></li>
            <li><a class="dropdown-item" href="#">Parceiros</a></li>
          </ul>
        </li>
      </ul>
       <!-- Navbar dropdown -->
       <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-mdb-toggle="dropdown"
            aria-expanded="false"
          >
            Lançamentos
          </a>
          <!-- Dropdown menu -->
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="<?= base_url('contas/pagar') ?>">Contas a Pagar</a></li>
            <li><a class="dropdown-item" href="<?= base_url('contas/receber') ?>">Contas a Receber</a></li>
            <li><a class="dropdown-item" href="#">Fluxo de Caixa</a></li>
          </ul>
        </li>
      </ul>
       <!-- Navbar dropdown -->
       <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-mdb-toggle="dropdown"
            aria-expanded="false"
          >
            Relatórios
          </a>
          <!-- Dropdown menu -->
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="#">Lançamentos por Período</a></li>
            <li><a class="dropdown-item" href="<?= base_url('contas/movimento') ?>">Movimento de Caixa</a></li>
            <li><a class="dropdown-item" href="#">Resumo anual</a></li>
          </ul>
        </li>
      </ul>
    </div>
    <!-- Collapsible wrapper -->
  </div>
  <!-- Container wrapper -->
</nav>
<!-- Navbar -->