<div class="container">
    <div class="d-flex justify-content-center align-items-center " style="height: 100vh;">
        <h2 class="p-4">Controle Financeiro Pessoal</h2>
        <form class="text-center border p-5" method="POST">
            <div>
                <h3 class="fw-bold mb-4">Entrar</h3>
            </div>
            <div class="form-outline mb-4">
                <input type="email" name="email" id="email" class="form-control" />
                <label class="form-label" for="email">Email</label>
            </div>
            <div class="form-outline mb-4">
                <input type="password" name="senha" id="senha" class="form-control" />
                <label class="form-label" for="senha">Senha</label>
            </div>
            <button type="submit" class="btn btn-primary btn-block">Enviar</button>
        <p class="red-text"><?= $error ? 'Dados de Acesso Incorretos.' : '' ?></p>
        </form>
        
    </div>
</div>