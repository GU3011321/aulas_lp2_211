<?php

class First extends TestCase{

    public function SetUp(){
        $this->resetInstance();
    }

    function testContaDeveInserirRegistroCorretamente(){
        $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
        $this->CI->builder->clear();
        $this->CI->builder->start();

        //ação
        $this->CI->load->library('conta');
        $res = $this->CI->conta->lista('pagar', 1, 2021);

        //verificação
        $this->assertEquals(3, sizeof($res));
        $this->assertEquals('Magalu', $res[0]['parceiro']);
        $this->assertEquals(2021, $res[0]['ano']);
        $this->assertEquals(1, $res[0]['mes']);

        $this->assertEquals(3, sizeof($res));
        $this->assertEquals('Casas Bahia', $res[1]['parceiro']);
        $this->assertEquals(2021, $res[1]['ano']);
        $this->assertEquals(1, $res[1]['mes']);

        $this->assertEquals(3, sizeof($res));
        $this->assertEquals('Conta de Energia', $res[2]['parceiro']);
        $this->assertEquals(2021, $res[2]['ano']);
        $this->assertEquals(1, $res[2]['mes']);
    }

    function testContaDeveInformarTotalDeContasAPagarEAReceber(){
        //cenário
        $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
        $this->CI->builder->clear();
        $this->CI->builder->start();
        
        //ação
        $this->CI->load->library('conta');
        $res = $this->CI->conta->total('pagar', 1, 2021);

        //verificação
        $this->assertEquals(5097.25, $res);
    }

    function testContaDeveCalcularSaldoMensal(){
        //cenário
        $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
        $this->CI->builder->clear();
        $this->CI->builder->start();

        //ação
        $this->CI->load->library('conta');
        $res = $this->CI->conta->saldo(1, 2021);

        //verificação
        $this->assertEquals(-875.07, $res);
    }
}


?>